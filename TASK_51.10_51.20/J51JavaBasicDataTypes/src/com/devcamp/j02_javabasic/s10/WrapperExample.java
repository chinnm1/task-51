package com.devcamp.j02_javabasic.s10;

import java.util.ArrayList;

public class WrapperExample {
    /***
     * Autoboxing là cơ chế tự động chuyển đổi kiểu dữ liệu nguyên thuỷ sang object
     * của Wrapper class tương ứng.
     */
    public static void autoBoxing() {
        char ch = 'a';
        // Autoboxing- primitive to Character object conversion
        Character a = ch;
        // printing the values from object
        System.out.println(a);

        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        // Autoboxing because ArrayList stores only objects
        arrayList.add(25);
        // printing the values from object
        System.out.println(arrayList.get(0));
    }

    /**
     * Unboxing là cơ chế giúp chuyển đổi các object của Wrapper class sang kiểu dữ
     * liệu nguyên thuỷ tương ứng
     */
    public static void unboxing() {
        Character ch = 'a';
        // unboxing - Character object to primitive conversion
        char a = ch;
        // printing the values from primitive data types
        System.out.println(a);

        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(24);
        // unboxing because get method returns an Integer object
        int num = arrayList.get(0);
        // printing the values from primitive data types
        System.out.println(num);
    }

    public static void main(String[] args) throws Exception {
        WrapperExample.autoBoxing();

        WrapperExample.unboxing();
    }
}
