package com.devcamp.j02_javabasic.s20;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class ForExample {
    public static void ToArray2() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        int[] results = arrayList.stream().mapToInt(i -> i).toArray();
        for (int i : results) {
            System.out.printf(i + " ");
        }
    }

    public static void ToArray1() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        Integer[] results = arrayList.stream().toArray(Integer[]::new);
        for (Integer i : results) {
            System.out.printf(i + " ");
        }
    }

    public static void ToArray() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        Integer[] results = arrayList.stream().toArray(size -> new Integer[size]);
        for (Integer i : results) {
            System.out.printf(i + " ");
        }
    }

    public static void arrayToArrayList2() {
        Integer[] arr = { 1, 2, 3, 4, 5, 6, 7 };
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (Integer i : arr) {
            arrayList.add(i);
        }
        System.out.println(arrayList);
    }

    public static void arrayToArrayList1() {
        Integer[] arr = { 1, 2, 3, 4, 5, 6, 7 };
        ArrayList<Integer> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, arr);
        System.out.println(arrayList);
    }

    public static void arrayToArrayList() {
        String[] strings = new String[] { "a", "b", "c", "d" };
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(strings));
        System.out.println(arrayList);
    }

    public static void newArrayList() {
        Random random = new Random();
        Integer[] arrayList = new Integer[20];
        // Create list number
        for (int i = 0; i < 20; i++) {
            arrayList[i] = Integer.valueOf(random.nextInt());
        }
        for (int i = 0; i < arrayList.length; i++) {
            System.out.println(arrayList[i]);
        }
    }

    public static void main(String[] args) throws Exception {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(25);
        arrayList.add(13);
        arrayList.add(35);
        arrayList.add(54);
        arrayList.add(56);
        arrayList.add(45);
        arrayList.add(58);
        arrayList.add(87);
        arrayList.add(65);
        arrayList.add(88);
        arrayList.add(37);
        arrayList.add(99);
        int index1 = arrayList.lastIndexOf(25);
        System.out.println("indexof" + index1);
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }
        // ForExample.newArrayList();
        // ForExample.arrayToArrayList();
        // ForExample.ToArray2();

    }
}
