import java.util.ArrayList;
import java.util.Arrays;

public class App {
    public static void MethodIndexOf() {
        String[] strings = new String[] { "ab", "bc", "cd", "da" };
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(strings));
        int index1 = arrayList.lastIndexOf("ab");
        System.out.println(index1);
    }

    public static void MethodAddIndex() {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(25);
        arrayList.add(13);
        arrayList.add(35);
        arrayList.add(54);
        arrayList.add(56);
        arrayList.add(45);

        System.out.println(arrayList);
    }

    public static void main(String[] args) throws Exception {
        App.MethodIndexOf();
    }
}
