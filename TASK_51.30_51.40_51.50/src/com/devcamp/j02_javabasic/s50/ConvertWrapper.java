package com.devcamp.j02_javabasic.s50;

public class ConvertWrapper {
    public static void autoBoxing() {
        byte bte = 10;
        short sh = 20;
        int it = 30;
        long lng = 40;
        float fat = 50.0F;
        double dbl = 60.0D;
        char ch = 'a';
        boolean bool = true;

        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;

        System.out.println("...Printing object values(In gias trij cuar object)");
        System.out.println("Byte subject" + byteobj);
        System.out.println("Short object" + shortobj);
        System.out.println("Integer object" + intobj);
        System.out.println("Long object" + longobj);
        System.out.println("Float object" + floatobj);
        System.out.println("Double object" + doubleobj);
        System.out.println("Character object" + charobj);
        System.out.println("Boolean object" + boolobj);

    }

    public static void unBoxing() {
        byte bte = 10;
        short sh = 20;
        int it = 30;
        long lng = 40;
        float fat = 50.0F;
        double dbl = 60.0D;
        char ch = 'a';
        boolean bool = true;

        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;

        byte bytevalue = byteobj;
        short shortvalue = shortobj;
        int intvalue = intobj;
        long longvalue = longobj;
        float floatvalue = floatobj;
        double doublevalue = doubleobj;
        char charvalue = charobj;
        boolean boolvalue = boolobj;

        System.out.println("...Printing primitives values(In gias trij cua primitives data types)");
        System.out.println("Byte value" + bytevalue);
        System.out.println("Short value" + shortvalue);
        System.out.println("Integer value" + intvalue);
        System.out.println("Long value" + longvalue);
        System.out.println("Floatvalue" + floatvalue);
        System.out.println("Double value" + doublevalue);
        System.out.println("Character value" + charvalue);
        System.out.println("Boolean value" + boolvalue);

    }

    public static void main(String[] args) {
        ConvertWrapper.autoBoxing();
        ConvertWrapper.unBoxing();
    }
}
